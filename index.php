<!DOCTYPE html>
<html lang="<?php echo Theme::lang() ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="generator" content="Bludit">
	<?php echo Theme::metaTags('title'); ?>
	<?php echo Theme::metaTags('description'); ?>
	<?php echo Theme::favicon('img/favicon.png'); ?>
	<?php echo Theme::cssBootstrap(); ?>
	<?php echo Theme::css('css/style.css'); ?>
	<?php Theme::plugins('siteHead'); ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY=" crossorigin="anonymous">
</head>
<body>

	<?php Theme::plugins('siteBodyBegin'); ?>
	<nav class="navbar navbar-expand-sm bg-custom">
	  <a class="navbar-brand" href="<?php echo Theme::siteUrl() ?>">
	      <img src="<?php echo $site->logo() ?>" width="30" height="30" class="d-inline-block align-top" alt="">
	  	<span class="text-white"><?php echo $site->title() ?></span><br>
	  </a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">

	<!-- Static pages -->
			<?php foreach ($staticContent as $staticPage): ?>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo $staticPage->permalink() ?>"><?php echo $staticPage->title() ?></a>
			</li>
			<?php endforeach ?>
				<?php foreach (Theme::socialNetworks() as $key=>$label): ?>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo $site->{$key}(); ?>" target="_blank">
					<i class="fa fa-<?php echo $key ?>" aria-hidden="true"></i>
				</a>
			</li>
			<?php endforeach; ?>
			</ul>
	  </div>
	</nav>

	<!-- Content -->
	<div class="container my-4">

		<div class="row">

			<!-- Blog Posts -->
			<div class="col-md-12">
			<?php
				if ($WHERE_AM_I == 'category' && $page->category()) {
					echo '<h4><span class="fa fa-tag mr-2"></span> '. $page->category() .'</h4>';
				}
                // Bludit content are pages
                // But if you order the content by date
                // These pages works as posts

                // $WHERE_AM_I variable detect where the user is browsing
                // If the user is watching a particular page/post the variable takes the value "page"
                // If the user is watching the frontpage the variable takes the value "home"
                if ($WHERE_AM_I == 'page') {
                    include(THEME_DIR_PHP.'page.php');
                } else {
                    include(THEME_DIR_PHP.'home.php');
                }
            ?>
			</div>

		</div>
	</div>

	<footer class="footer bg-custom">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="sidebar-wrap">
						<?php Theme::plugins('siteSidebar') ?>
					</div>
				</div>
				<div class="col-md-4">
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<p class="footer-text m-0 text-center text-white text-uppercase"><?php echo $site->footer(); ?><span class="ml-5 text-warning">Powered by<img class="mini-logo" src="<?php echo DOMAIN_THEME_IMG.'favicon.png'; ?>"/><a target="_blank" class="text-white" href="https://www.bludit.com">Bludit</a></span></p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Javascript -->
	<?php
        // Include Jquery file from Bludit Core
        echo Theme::jquery();

        // Include javascript Bootstrap file from Bludit Core
		echo Theme::jsBootstrap();


    ?>
	
	
	<!-- Load Bludit Plugins: Site Body End -->
	<?php Theme::plugins('siteBodyEnd'); ?>

</body>
</html>
